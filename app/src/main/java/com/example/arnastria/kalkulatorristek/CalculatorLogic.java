package com.example.arnastria.kalkulatorristek;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Arrays;

public class CalculatorLogic {
    public CalculatorLogic() {
    }

    public String calculate(String all) {
    if (!Character.isDigit(all.charAt(all.length() - 1))) {
        return "SYNTAX ERROR";
    }
    String[] numbers = all.split("[\\^/*+-]");
    try {
        if (numbers.length < 2) {
            return all;
        } else {
            String[] operators = all.replaceAll("\\d", "").split("");
            ArrayList<String> nums = new ArrayList<>(Arrays.asList(numbers));
            ArrayList<String> ops = new ArrayList<>(Arrays.asList(operators));
            ops.remove(".");
            ops.remove("");
            int indexMult = ops.indexOf("*");
            int indexDiv = ops.indexOf("/");
            int indexAdd = ops.indexOf("+");
            int indexSub = ops.indexOf("-");
            int indexPow = ops.indexOf("^");

            if (indexPow != -1) {
              double resPow = Math.pow(Double.parseDouble(nums.get(indexPow)), Double.parseDouble(nums.get(indexPow + 1)));
              String inserted = Double.toString(resPow);
              String replaced = nums.get(indexPow) + "^" + nums.get(indexPow + 1);
              all = all.replace(replaced, inserted);
              return calculate(all);
            }

            if (indexMult != -1 && (indexMult < indexDiv || indexDiv == -1)) {
                double resMult = Double.parseDouble(nums.get(indexMult)) * Double.parseDouble(nums.get(indexMult + 1));
                String inserted = Double.toString(resMult);
                String replaced = nums.get(indexMult) + "*" + nums.get(indexMult + 1);
                all = all.replace(replaced, inserted);
                return calculate(all);
            }
  
            if (indexDiv != -1 && (indexDiv < indexMult || indexMult == -1)) {
                double resDiv = Double.parseDouble(nums.get(indexDiv)) / Double.parseDouble(nums.get(indexDiv + 1));
                String inserted = Double.toString(resDiv);
                if (inserted.split("\\.")[1].length() > 8)
                    inserted = String.format(Locale.ENGLISH, "%.8f", resDiv);
                String replaced = nums.get(indexDiv) + "/" + nums.get(indexDiv + 1);
                all = all.replace(replaced, inserted);
                return calculate(all);
            }
  
            if (indexAdd != -1) {
                double resAdd = Double.parseDouble(nums.get(indexAdd)) + Double.parseDouble(nums.get(indexAdd + 1));
                String inserted = Double.toString(resAdd);
                String replaced = nums.get(indexAdd) + "+" + nums.get(indexAdd + 1);
                all = all.replace(replaced, inserted);
                return calculate(all);
            }
  
            if (indexSub != -1) {
                double resSub = Double.parseDouble(nums.get(indexSub)) - Double.parseDouble(nums.get(indexSub + 1));
                String inserted = Double.toString(resSub);
                String replaced = nums.get(indexSub) + "-" + nums.get(indexSub + 1);
                all = all.replace(replaced, inserted);
                return calculate(all);
            }
            return "";
        }
    } catch (Exception e) {
        return "SYNTAX ERROR";
    }
  
  }  
}