package com.example.arnastria.kalkulatorristek;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CalculatorPage extends AppCompatActivity {
    private Button[] buttons;
    private Button operatorPlus, operatorMinus, operatorMultiply, operatorDiv, comma, equalsButton, clear;
    private TextView operatorTextField, resultTextField;
    private CalculatorLogic logic = new CalculatorLogic();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_page);
        assignViewAndButton();

    }

    void assignViewAndButton(){
        operatorTextField = (TextView) findViewById(R.id.formula);
        resultTextField = (TextView) findViewById(R.id.result);

        operatorPlus = (Button) findViewById(R.id.btn_plus);
        operatorMinus = (Button) findViewById(R.id.btn_minus);
        operatorMultiply = (Button) findViewById(R.id.btn_multiply);
        operatorDiv = (Button) findViewById(R.id.btn_divide);
        comma = (Button) findViewById(R.id.btn_decimal);
        equalsButton = (Button) findViewById(R.id.btn_equals);
        clear = (Button) findViewById(R.id.btn_clear);
        buttons = new Button[]{
                (Button) findViewById(R.id.btn_0),
                (Button) findViewById(R.id.btn_1),
                (Button) findViewById(R.id.btn_2),
                (Button) findViewById(R.id.btn_3),
                (Button) findViewById(R.id.btn_4),
                (Button) findViewById(R.id.btn_5),
                (Button) findViewById(R.id.btn_6),
                (Button) findViewById(R.id.btn_7),
                (Button) findViewById(R.id.btn_8),
                (Button) findViewById(R.id.btn_9),
                operatorPlus,operatorDiv,operatorMultiply,operatorMinus,comma
        };

        for(final Button button : buttons){
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String resultText;
                    if(resultTextField.getText() == null) {
                        resultText = "";
                    }else if(resultTextField.getText().equals("SYNTAX ERROR")){
                        resultText = "";
                    } else {
                        resultText = resultTextField.getText().toString();
                    }
                    resultText+=button.getText();
                    resultTextField.setText(resultText);
                }
            });
        }

        equalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = "";
                try{
                    result= logic.calculate(resultTextField.getText().toString());
                } catch (Exception e){
                    result = "SYNTAX ERROR";
                }
                resultTextField.setText(result);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultTextField.setText("");
            }
        });
    }
}
