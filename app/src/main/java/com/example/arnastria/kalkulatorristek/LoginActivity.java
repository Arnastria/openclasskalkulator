package com.example.arnastria.kalkulatorristek;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


public class LoginActivity extends AppCompatActivity {
    SharedPreferences sharedpreferences;
    Button loginButton;
    EditText username;
    EditText password;
    ProgressBar progressBarLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.login_EditText_Username);
        password = (EditText) findViewById(R.id.login_EditText_Password);
        loginButton = (Button) findViewById(R.id.login_buttonLogin);
        progressBarLogin = (ProgressBar) findViewById(R.id.progress_bar_login);

        sharedpreferences = getSharedPreferences("userData",
                Context.MODE_PRIVATE);

        if (!sharedpreferences.getString("username", "").equals("")) {
            Intent intent = new Intent(LoginActivity.this, CalculatorPage.class);
            startActivity(intent);
        }

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameInput = username.getText().toString();
                String passwordInput = password.getText().toString();
                if(isEmpty(usernameInput,passwordInput)){
                    Toast.makeText(LoginActivity.this,
                            "Username atau Password tidak boleh kosong",Toast.LENGTH_SHORT).show();
                    return;
                }
                progressBarLogin.setVisibility(View.VISIBLE);
                Login(usernameInput,passwordInput);
            }
        });

    }

    boolean isEmpty(String username,String password){
        return username.equals("")||password.equals("");
    }

    void Login(String username, String password) {
        if (username.equals("mobdev") && password.equals("ristek2018")) {
            progressBarLogin.setVisibility(View.INVISIBLE);
            SharedPreferences.Editor editor = sharedpreferences.edit();

            editor.putString("username", username);
            editor.commit();
            Toast.makeText(LoginActivity.this, "shared "+sharedpreferences.getString("username",""), Toast.LENGTH_LONG).show();

            Intent intent = new Intent(LoginActivity.this, CalculatorPage.class);
            startActivity(intent);
            finish();
        } else {
            progressBarLogin.setVisibility(View.INVISIBLE);
            Toast.makeText(LoginActivity.this, "Login Failed, make sure you input the correct username/password", Toast.LENGTH_LONG).show();
        }
    }
}
